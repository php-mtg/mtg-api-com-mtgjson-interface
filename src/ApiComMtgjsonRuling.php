<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgjson-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgjson;

use DateTimeInterface;

/**
 * ApiComMtgjsonRuling class file.
 * 
 * This represents a ruling that was issued to clarify rules statements
 * about a specific card.
 * 
 * @author Anastaszor
 */
class ApiComMtgjsonRuling
{
	
	/**
	 * The date when the ruling was applied.
	 * 
	 * @var ?DateTimeInterface
	 */
	public ?DateTimeInterface $date = null;
	
	/**
	 * The text of the ruling.
	 * 
	 * @var ?string
	 */
	public ?string $text = null;
	
}
