<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgjson-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgjson;

/**
 * ApiComMtgjsonDeckMeta class file.
 * 
 * This represents a metadata wrapper around a single deck.
 * 
 * @author Anastaszor
 */
class ApiComMtgjsonDeckMeta
{
	
	/**
	 * The metadata.
	 *
	 * @var ?ApiComMtgjsonVersion
	 */
	public ?ApiComMtgjsonVersion $meta = null;
	
	/**
	 * The deck.
	 * 
	 * @var ?ApiComMtgjsonDeck
	 */
	public ?ApiComMtgjsonDeck $data = null;
	
	
}
