<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgjson-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgjson;

class ApiComMtgjsonTcgplayerSkus
{
	
	/**
	 * The condition of the card.
	 *
	 * Example:
	 * "DAMAGED", "HEAVILY_PLAYED", "LIGHTLY_PLAYED",
	 * "MODERATELY_PLAYED", "NEAR_MINT", "UNOPENED"
	 *
	 * @var ?string
	 */
	public ?string $condition = null;
	
	/**
	 * The finishes of the card.
	 * 
	 * Example:
	 * "FOIL_ETCHED"
	 * 
	 * @var array<integer, string>
	 */
	public array $finishes = [];
	
	/**
	 * The language of the card.
	 *
	 * Example:
	 * "CHINESE_SIMPLIFIED", "CHINESE_TRADITIONAL", "ENGLISH", "FRENCH",
	 * "GERMAN", "ITALIAN", "JAPANESE", "KOREAN", "PORTUGUESE_BRAZIL",
	 * "RUSSIAN", "SPANISH"
	 *
	 * @var ?string
	 */
	public ?string $language = null;
	
	/**
	 * The printing style of the card.
	 *
	 * Example:
	 * "FOIL", "NON_FOIL"
	 *
	 * @return ?string
	 */
	public ?string $printing = null;
	
	/**
	 * The product identifier of the card.
	 *
	 * @return ?string
	 */
	public ?string $productId = null;
	
	/**
	 * The SKU identifier of the card.
	 *
	 * @return ?string
	 */
	public ?string $skuId = null;
	
}
