<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgjson-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgjson;

/**
 * ApiComMtgjsonSealedProductContentDeckCollection class file.
 * 
 * This represents a collection of decks in a specified configuration.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.LongClassName")
 */
class ApiComMtgjsonSealedProductContentDeckCollection
{
	
	/**
	 * The decks of this collection.
	 * 
	 * @var array<integer, ApiComMtgjsonSealedProductContentDeck>
	 */
	public array $deck = [];
	
}
