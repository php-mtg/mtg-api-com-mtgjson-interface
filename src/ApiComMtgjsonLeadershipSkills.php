<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgjson-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgjson;

/**
 * ApiComMtgjsonLeadershipSkills class file.
 * 
 * This represents the leadership skills in the different formats for a
 * specific card.
 * 
 * @author Anastaszor
 */
class ApiComMtgjsonLeadershipSkills
{
	
	/**
	 * Is this card able to be your commander in brawl?
	 * 
	 * @var ?boolean
	 */
	public ?bool $brawl = null;
	
	/**
	 * Is this card able to be your commander in commander?
	 * 
	 * @var ?boolean
	 */
	public ?bool $commander = null;
	
	/**
	 * Is this card able to be your commander in oathbreaker?
	 * 
	 * @var ?boolean
	 */
	public ?bool $oathbreaker = null;
	
}
