<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgjson-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgjson;

use PhpExtended\Uuid\UuidInterface;

/**
 * ApiComMtgjsonSealedProductContentCard class file.
 * 
 * This  represents a single sealed card.
 * 
 * @author Anastaszor
 */
class ApiComMtgjsonSealedProductContentCard
{
	
	/**
	 * Whether this card is foil.
	 * 
	 * @var ?boolean
	 */
	public ?bool $foil = null;
	
	/**
	 * The name of this content Card.
	 * 
	 * @var ?string
	 */
	public ?string $name = null;
	
	/**
	 * The number of the card.
	 * 
	 * @var ?integer
	 */
	public ?int $number = null;
	
	/**
	 * The code of the related set.
	 * 
	 * @var ?string
	 */
	public ?string $set = null;
	
	/**
	 * The uuid of this content Card.
	 * 
	 * @var ?UuidInterface
	 */
	public ?UuidInterface $uuid = null;
	
}
