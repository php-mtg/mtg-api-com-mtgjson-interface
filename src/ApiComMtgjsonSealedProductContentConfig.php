<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgjson-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgjson;

/**
 * ApiComMtgjsonSealedProductContentConfig class file.
 *
 * This represents a collection of decks in a specified configuration.
 *
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.LongClassName")
 */
class ApiComMtgjsonSealedProductContentConfig
{
	
	/**
	 * Individual cards present in this configuration.
	 * 
	 * @var array<integer, ApiComMtgjsonSealedProductContentCard>
	 */
	public array $card = [];
	
	/**
	 * The decks present in this configuration.
	 *
	 * @var array<integer, ApiComMtgjsonSealedProductContentDeck>
	 */
	public array $deck = [];
	
	/**
	 * The sealed products present in this configuration.
	 * 
	 * @var array<integer, ApiComMtgjsonSealedProductContentSealed>
	 */
	public array $sealed = [];
	
}
