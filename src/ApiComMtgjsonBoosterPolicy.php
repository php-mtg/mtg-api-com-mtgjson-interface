<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgjson-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgjson;

/**
 * ApiComMtgjsonBoosterPolicy class file.
 * 
 * This represents a booster policy.
 * 
 * @author Anastaszor
 */
class ApiComMtgjsonBoosterPolicy
{
	
	/**
	 * The boosters that are used in this policy.
	 * 
	 * @var array<integer, ApiComMtgjsonBooster>
	 */
	public array $boosters = [];
	
	/**
	 * The total weight.
	 * 
	 * @var ?integer
	 */
	public ?int $boostersTotalWeight = null;
	
	/**
	 * The name of the policy.
	 * 
	 * @var ?string
	 */
	public ?string $name = null;
	
	/**
	 * The sheets this booster is based on.
	 * 
	 * @var array<string, ApiComMtgjsonBoosterSheet>
	 */
	public array $sheets = [];
	
}
