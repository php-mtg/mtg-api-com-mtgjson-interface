<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgjson-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgjson;

/**
 * ApiComMtgjsonKeywords class file.
 * 
 * This class represents the list of keywords that may be used by the api.
 * 
 * @author Anastaszor
 */
class ApiComMtgjsonKeywords
{
	
	/**
	 * A list of ability words found in rules text on cards.
	 *
	 * Example:
	 * "Adamant", "Addendum", "Alliance", "Battalion", "Bloodrush", "Channel",
	 * "Chroma", "Cohort", "Constellation", "Converge", "Corrupted",
	 * "Council's dilemma", "Coven", "Delirium", "Domain", "Eminence",
	 * "Enrage", "Fateful hour", "Ferocious", "Formidable", "Grandeur",
	 * "Hellbent", "Hero's Reward", "Heroic", "Imprint", "Inspired",
	 * "Join forces", "Kinfall", "Kinship", "Landfall", "Landship", "Legacy",
	 * "Lieutenant", "Magecraft", "Metalcraft", "Morbid", "Pack tactics",
	 * "Parley", "Radiance", "Raid", "Rally", "Revolt", "Secret council",
	 * "Spell mastery", "Strive", "Sweep", "Teamwork", "Tempting offer",
	 * "Threshold", "Underdog", "Undergrowth", "Will of the council"
	 *
	 * @var array<integer, string>
	 */
	public array $abilityKeywords = [];
	
	/**
	 * A list of keyword abilities found in rules text on cards.
	 *
	 * Example:
	 * "Absorb", "Affinity", "Afflict", "Afterlife", "Aftermath", "Amplify",
	 * "Annihilator", "Ascend", "Assist", "Augment", "Aura Swap", "Awaken",
	 * "Backup", "Banding", "Basic landcycling", "Battle Cry", "Bestow",
	 * "Blitz", "Bloodthirst", "Boast", "Bushido", "Buyback", "Cascade",
	 * "Casualty", "Champion", "Changeling", "Cipher", "Cleave",
	 * "Commander ninjutsu", "Companion", "Compleated", "Conspire", "Convoke",
	 * "Crew", "Cumulative upkeep", "Cycling", "Dash", "Daybound",
	 * "Deathtouch", "Decayed", "Defender", "Delve", "Demonstrate",
	 * "Desertwalk", "Dethrone", "Devoid", "Devour", "Disturb",
	 * "Double agenda", "Double strike", "Dredge", "Echo", "Embalm", "Emerge",
	 * "Enchant", "Encore", "Enlist", "Entwine", "Epic", "Equip", "Escalate",
	 * "Escape", "Eternalize", "Evoke", "Evolve", "Exalted", "Exploit",
	 * "Extort", "Fabricate", "Fading", "Fear", "First strike", "Flanking",
	 * "Flash", "Flashback", "Flying", "For Mirrodin!", "Forecast",
	 * "Forestcycling", "Forestwalk", "Foretell", "Fortify", "Frenzy",
	 * "Friends forever", "Fuse", "Graft", "Gravestorm", "Haste", "Haunt",
	 * "Hexproof", "Hexproof from", "Hidden agenda", "Hideaway",
	 * "Horsemanship", "Improvise", "Indestructible", "Infect", "Ingest",
	 * "Intensity", "Intimidate", "Islandcycling", "Islandwalk", "Jump-start",
	 * "Kicker", "Landcycling", "Landwalk", "Legendary landwalk", "Level Up",
	 * "Lifelink", "Living metal", "Living weapon", "Madness", "Megamorph",
	 * "Melee", "Menace", "Mentor", "Miracle", "Modular",
	 * "More Than Meets the Eye", "Morph", "Mountaincycling", "Mountainwalk",
	 * "Multikicker", "Mutate", "Myriad", "Nightbound", "Ninjutsu",
	 * "Nonbasic landwalk", "Offering", "Outlast", "Overload", "Partner",
	 * "Partner with", "Persist", "Phasing", "Plainscycling", "Plainswalk",
	 * "Poisonous", "Protection", "Prototype", "Provoke", "Prowess", "Prowl",
	 * "Rampage", "Ravenous", "Reach", "Read Ahead", "Rebound", "Reconfigure",
	 * "Recover", "Reinforce", "Renown", "Replicate", "Retrace", "Riot",
	 * "Ripple", "Scavenge", "Shadow", "Shroud", "Skulk", "Slivercycling",
	 * "Soulbond", "Soulshift", "Specialize", "Spectacle", "Splice",
	 * "Split second", "Squad", "Storm", "Sunburst", "Surge", "Suspend",
	 * "Swampcycling", "Swampwalk", "Totem armor", "Toxic", "Training",
	 * "Trample", "Transfigure", "Transmute", "Tribute", "Typecycling",
	 * "Undaunted", "Undying", "Unearth", "Unleash", "Vanishing", "Vigilance",
	 * "Ward", "Wither", "Wizardcycling"
	 *
	 * @var array<integer, string>
	 */
	public array $keywordAbilities = [];
	
	/**
	 * A list of keyword actions found in rules text on cards.
	 *
	 * Example:
	 * "Abandon", "Activate", "Adapt", "Amass", "Assemble", "Attach",
	 * "Bolster", "Cast", "Clash", "Conjure", "Connive", "Convert", "Counter",
	 * "Create", "Destroy", "Detain", "Discard", "Double", "Exchange",
	 * "Exert", "Exile", "Explore", "Fateseal", "Fight", "Food", "Goad",
	 * "Incubate", "Investigate", "Learn", "Manifest", "Meld", "Mill",
	 * "Monstrosity", "Open an Attraction", "Planeswalk", "Play", "Populate",
	 * "Proliferate", "Regenerate", "Reveal", "Roll to Visit Your Attractions",
	 * "Sacrifice", "Scry", "Seek", "Set in motion", "Shuffle", "Support",
	 * "Surveil", "Tap", "Transform", "Treasure", "Untap",
	 * "Venture into the dungeon", "Vote"
	 *
	 * @var array<integer, string>
	 */
	public array $keywordActions = [];
	
}
