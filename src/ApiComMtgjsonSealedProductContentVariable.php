<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgjson-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgjson;

/**
 * ApiComMtgjsonSealedProductContentVariable class file.
 * 
 * This  represents a variable (logical OR) section of content.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.LongClassName")
 */
class ApiComMtgjsonSealedProductContentVariable
{
	
	/**
	 * The card count.
	 * 
	 * @var ?integer
	 */
	public ?int $cardCount = null;
	
	/**
	 * The configs.
	 * 
	 * @var array<integer, ApiComMtgjsonSealedProductContentConfig>
	 */
	public array $configs = [];
	
	/**
	 * The deck contents.
	 *
	 * @var array<integer, ApiComMtgjsonSealedProductContentDeck>
	 */
	public array $deck = [];
	
}
