<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgjson-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgjson;

/**
 * ApiComMtgjsonSetTranslation class file.
 * 
 * This represents the translation information on a set.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.TooManyFields")
 */
class ApiComMtgjsonSetTranslation
{
	
	/**
	 * The ancient greek.
	 * 
	 * @var ?string
	 */
	public ?string $ancientGreek = null;
	
	/**
	 * The arabic translation.
	 * 
	 * @var ?string
	 */
	public ?string $arabic = null;
	
	/**
	 * The chinese simplified translation.
	 * 
	 * @var ?string
	 */
	public ?string $chineseSimplified = null;
	
	/**
	 * The chinese traditional translation.
	 * 
	 * @var ?string
	 */
	public ?string $chineseTraditional = null;
	
	/**
	 * The french translation.
	 * 
	 * @var ?string
	 */
	public ?string $french = null;
	
	/**
	 * The german translation.
	 * 
	 * @var ?string
	 */
	public ?string $german = null;
	
	/**
	 * The hebrew translation.
	 * 
	 * @var ?string
	 */
	public ?string $hebrew = null;
	
	/**
	 * The italian translation.
	 * 
	 * @var ?string
	 */
	public ?string $italian = null;
	
	/**
	 * The japanese translation.
	 * 
	 * @var ?string
	 */
	public ?string $japanese = null;
	
	/**
	 * The korean translation.
	 * 
	 * @var ?string
	 */
	public ?string $korean = null;
	
	/**
	 * The latin translation.
	 * 
	 * @var ?string
	 */
	public ?string $latin = null;
	
	/**
	 * The phyrexian translation.
	 * 
	 * @var ?string
	 */
	public ?string $phyrexian = null;
	
	/**
	 * The portuguese brazil translation.
	 * 
	 * @var ?string
	 */
	public ?string $portugueseBrazil = null;
	
	/**
	 * The russian translation.
	 * 
	 * @var ?string
	 */
	public ?string $russian = null;
	
	/**
	 * The sanskrit translation.
	 * 
	 * @var ?string
	 */
	public ?string $sanskrit = null;
	
	/**
	 * The spanish translation.
	 * 
	 * @var ?string
	 */
	public ?string $spanish = null;
	
}
