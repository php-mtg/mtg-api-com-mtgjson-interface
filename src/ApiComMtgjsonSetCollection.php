<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgjson-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgjson;

/**
 * ApiComMtgjsonSetCollection class file.
 * 
 * This represents a collection of sets.
 * 
 * @author Anastaszor
 */
class ApiComMtgjsonSetCollection
{
	
	/**
	 * Gets the version of the set collection.
	 *
	 * @var ?ApiComMtgjsonVersion
	 */
	public ?ApiComMtgjsonVersion $meta = null;
	
	/**
	 * The sets of this collection.
	 *
	 * @var array<integer, ApiComMtgjsonSetResume>
	 */
	public array $data = [];
	
}
