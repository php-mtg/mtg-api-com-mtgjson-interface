<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgjson-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgjson;

/**
 * ApiComMtgjsonCardType class file.
 * 
 * This represents a card type and its dependancies.
 * 
 * @author Anastaszor
 */
class ApiComMtgjsonCardType
{
	
	/**
	 * The name of the type.
	 * 
	 * @var ?string
	 */
	public ?string $name = null;
	
	/**
	 * The list of subtypes.
	 * 
	 * @var array<integer, string>
	 */
	public array $subtypes = [];
	
	/**
	 * The list of supertypes.
	 * 
	 * @var array<integer, string>
	 */
	public array $supertypes = [];
	
}
