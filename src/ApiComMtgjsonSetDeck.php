<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgjson-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgjson;

use PhpExtended\Uuid\UuidInterface;

/**
 * ApiComMtgjsonSetDeck class file.
 * 
 * This represents a deck that is tied as sealed product for a given set.
 * 
 * @author Anastaszor
 */
class ApiComMtgjsonSetDeck
{
	
	/**
	 * The card quantities present in each deck.
	 * 
	 * @var array<integer, ApiComMtgjsonSetDeckCardQuantity>
	 */
	public array $cards = [];
	
	/**
	 * The name of this deck.
	 * 
	 * @var ?string
	 */
	public ?string $name = null;
	
	/**
	 * The uuid of the related sealed products.
	 * 
	 * @var array<integer, UuidInterface>
	 */
	public array $sealedProductUuids = [];
	
}
