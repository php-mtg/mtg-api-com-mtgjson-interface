<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgjson-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgjson;

use PhpExtended\Uuid\UuidInterface;

/**
 * ApiComMtgjsonCardPrice class file.
 * 
 * This represents the collection of prices for a given card.
 * 
 * @author Anastaszor
 */
class ApiComMtgjsonCardPrice
{
	
	/**
	 * Gets the uuid of the card.
	 * 
	 * @var ?UuidInterface
	 */
	public ?UuidInterface $uuid = null;
	
	/**
	 * Gets the price for the non foil mtgo cards.
	 * 
	 * @var array<integer, ApiComMtgjsonPrice>
	 */
	public array $mtgoPrices = [];
	
	/**
	 * Gets the price for the foil mtgo cards.
	 * 
	 * @var array<integer, ApiComMtgjsonPrice>
	 */
	public array $mtgoFoilPrices = [];
	
	/**
	 * Gets the price for the non foil paper cards.
	 * 
	 * @var array<integer, ApiComMtgjsonPrice>
	 */
	public array $paperPrices = [];
	
	/**
	 * Gets the price for the non foil paper cards.
	 * 
	 * @var array<integer, ApiComMtgjsonPrice>
	 */
	public array $paperFoilPrices = [];
	
}
