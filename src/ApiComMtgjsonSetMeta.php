<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgjson-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgjson;

/**
 * ApiComMtgjsonSetMeta class file.
 * 
 * This represents a metadata wrapper around a single set.
 * 
 * @author Anastaszor
 */
class ApiComMtgjsonSetMeta
{
	
	/**
	 * The meta about the set.
	 * 
	 * @var ?ApiComMtgjsonVersion
	 */
	public ?ApiComMtgjsonVersion $meta = null;
	
	/**
	 * The set.
	 *
	 * @var ?ApiComMtgjsonSet
	 */
	public ?ApiComMtgjsonSet $data = null;
	
}
