<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgjson-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgjson;

use DateTimeInterface;
use PhpExtended\Uuid\UuidInterface;

/**
 * ApiComMtgjsonCard class file.
 * 
 * This represents a full featured-card with all its faces.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ExcessivePublicCount")
 * @SuppressWarnings("PHPMD.TooManyFields")
 */
class ApiComMtgjsonCard
{
	
	/**
	 * Name of the artist that illustrated the card art.
	 * 
	 * @var ?string
	 */
	public ?string $artist = null;
	
	/**
	 * The id of the related artists.
	 * 
	 * @var array<integer, UuidInterface>
	 */
	public array $artistIds = [];
	
	/**
	 * The name of the card, reduced to ascii characters.
	 * 
	 * @var ?string
	 */
	public ?string $asciiName = null;
	
	/**
	 * The attraction lights of this card.
	 * 
	 * @var array<integer, integer>
	 */
	public array $attractionLights = [];
	
	/**
	 * List of the card's available printing types.
	 * 
	 * @var array<integer, string>
	 */
	public array $availability = [];
	
	/**
	 * The booster types this card can be found in.
	 * 
	 * @var array<integer, string>
	 */
	public array $boosterTypes = [];
	
	/**
	 * Color of the border. Can be black, borderless, gold, silver, or white.
	 * 
	 * @var ?string
	 */
	public ?string $borderColor = null;
	
	/**
	 * List of all related card part names.
	 * 
	 * @var array<integer, string>
	 */
	public array $cardParts = [];
	
	/**
	 * List of all colors in card’s mana cost, rules text and any color indicator.
	 * 
	 * @var array<integer, string>
	 */
	public array $colorIdentity = [];
	
	/**
	 * List of all colors in card's color indicator (The symbol showing the
	 * colors of the card).
	 * 
	 * @var array<integer, string>
	 */
	public array $colorIndicator = [];
	
	/**
	 * List of all colors in card’s mana cost and any color indicator. Some
	 * cards are special (such as Devoid cards or other cards with certain
	 * rules text).
	 * 
	 * @var array<integer, string>
	 */
	public array $colors = [];
	
	/**
	 * The converted mana cost of the card.
	 * 
	 * @var ?integer
	 */
	public ?int $convertedManaCost = null;
	
	/**
	 * Converted mana cost. Always a number. NOTE: cmc may have a decimal point
	 * as cards from unhinged may contain "half mana" (such as 'Little Girl'
	 * with a cmc of 0.5). Cards without this field have an implied cmc of zero
	 * as per rule 202.3a.
	 * 
	 * @var ?float
	 */
	public ?float $manaValue = null;
	
	/**
	 * How many of this card exists in a relevant deck.
	 * 
	 * @var ?integer
	 */
	public ?int $count = null;
	
	/**
	 * The defense value (for battles).
	 * 
	 * @var ?string
	 */
	public ?string $defense = null;
	
	/**
	 * If the card is in a duel deck product, can be a(left) or b(right).
	 * 
	 * @var ?string
	 */
	public ?string $duelDeck = null;
	
	/**
	 * Card rank on EDHRec. https://www.edhrec.com/.
	 * 
	 * @var ?integer
	 */
	public ?int $edhrecRank = null;
	
	/**
	 * Card saltiness on EDHRec.
	 * 
	 * @var ?float
	 */
	public ?float $edhrecSaltiness = null;
	
	/**
	 * The mana value (=converted mana cost) of the face of either half or part
	 * of the card.
	 * 
	 * @var ?float
	 */
	public ?float $faceManaValue = null;
	
	/**
	 * Name on the face of the card.
	 * 
	 * @var ?string
	 */
	public ?string $faceName = null;
	
	/**
	 * The finishes of the card.
	 * 
	 * @var array<integer, string>
	 */
	public array $finishes = [];
	
	/**
	 * The secondary name of the card, if any.
	 * 
	 * @var ?string
	 */
	public ?string $flavorName = null;
	
	/**
	 * The flavor name of the recto face, if any.
	 * 
	 * @var ?string
	 */
	public ?string $faceFlavorName = null;
	
	/**
	 * Italicized text found below the rules text that has no game function.
	 * 
	 * @var ?string
	 */
	public ?string $flavorText = null;
	
	/**
	 * Foreign language names for the card, if this card in this set was
	 * printed in another language. An array of objects, each object having
	 * 'language', 'name' and 'multiverseid' keys. Not available for all sets.
	 * 
	 * @var array<integer, ApiComMtgjsonForeignData>
	 */
	public array $foreignData = [];
	
	/**
	 * Values can be colorshifted, compasslanddfc, devoid, draft, legendary, 
	 * miracle, mooneldrazidfc, nyxtouched, originpwdfc, sunmoondfc, or tombstone.
	 * 
	 * @var array<integer, string>
	 */
	public array $frameEffects = [];
	
	/**
	 * Version of the card frame style. Can be 1993, 1997, 2003, 2015, or future.
	 * 
	 * @var ?string
	 */
	public ?string $frameVersion = null;
	
	/**
	 * Maximum hand size modifier. Only exists for Vanguard cards.
	 * 
	 * @var ?integer
	 */
	public ?int $hand = null;
	
	/**
	 * Has the card a content warning.
	 * 
	 * @var ?boolean
	 */
	public ?bool $hasContentWarning = null;
	
	/**
	 * Can the card be found in foil?
	 * 
	 * @var ?boolean
	 */
	public ?bool $hasFoil = null;
	
	/**
	 * If the card allows a value other than 4 copies in a deck.
	 * 
	 * @var ?boolean
	 */
	public ?bool $hasAlternativeDeckLimit = null;
	
	/**
	 * Can the card be found in non-foil?
	 *
	 * @var ?boolean
	 */
	public ?bool $hasNonFoil = null;
	
	/**
	 * See the Identifiers data model.
	 * 
	 * @var ?ApiComMtgjsonIdentifier
	 */
	public ?ApiComMtgjsonIdentifier $identifiers = null;
	
	/**
	 * The card has some kind of alternative variation to its printed counterpart.
	 * 
	 * @var ?boolean
	 */
	public ?bool $isAlternative = null;
	
	/**
	 * Is the card foil ?
	 * 
	 * @var ?boolean
	 */
	public ?bool $isFoil = null;
	
	/**
	 * Is the card full artwork?
	 * 
	 * @var ?boolean
	 */
	public ?bool $isFullArt = null;
	
	/**
	 * Is the card funny.
	 * 
	 * @var ?boolean
	 */
	public ?bool $isFunny = null;
	
	/**
	 * Is the card available on mtgo ?
	 * 
	 * @var ?boolean
	 */
	public ?bool $isMtgo = null;
	
	/**
	 * Is the card only available online?
	 * 
	 * @var ?boolean
	 */
	public ?bool $isOnlineOnly = null;
	
	/**
	 * Is the card oversized?
	 * 
	 * @var ?boolean
	 */
	public ?bool $isOversized = null;
	
	/**
	 * Is the card a promotional print?
	 * 
	 * @var ?boolean
	 */
	public ?bool $isPromo = null;
	
	/**
	 * Whether this card has been rebalanced.
	 * 
	 * @var ?boolean
	 */
	public ?bool $isRebalanced = null;
	
	/**
	 * Has the card been reprinted?
	 * 
	 * @var ?boolean
	 */
	public ?bool $isReprint = null;
	
	/**
	 * Is the card on the MTG Reserved List?
	 * 
	 * @var ?boolean
	 */
	public ?bool $isReserved = null;
	
	/**
	 * Is this card found in a booster pack?
	 * 
	 * @var ?boolean
	 */
	public ?bool $isStarter = null;
	
	/**
	 * Does the card have a story spotlight?
	 * 
	 * @var ?boolean
	 */
	public ?bool $isStorySpotlight = null;
	
	/**
	 * Does the card normally have a text box, but doesn't on this card?
	 * 
	 * @var ?boolean
	 */
	public ?bool $isTextless = null;
	
	/**
	 * If this card was a timeshifted card in the set.
	 * 
	 * @var ?boolean
	 */
	public ?bool $isTimeshifted = null;
	
	/**
	 * The keywords found on the card.
	 * 
	 * @var array<integer, string>
	 */
	public array $keywords = [];
	
	/**
	 * The language of the card.
	 * 
	 * @var ?string
	 */
	public ?string $language = null;
	
	/**
	 * Type of card layout. Can be normal, split, flip, transform, meld, 
	 * leveler, saga, planar, scheme, vanguard, token, double_faced_token, 
	 * emblem, augment, aftermath, or host.
	 * 
	 * @var ?string
	 */
	public ?string $layout = null;
	
	/**
	 * See the leadershipSkills structure. Will be included only if one value is true.
	 * 
	 * @var ?ApiComMtgjsonLeadershipSkills
	 */
	public ?ApiComMtgjsonLeadershipSkills $leadershipSkills = null;
	
	/**
	 * List of play formats that are legal for a specific card.
	 * 
	 * @var ?ApiComMtgjsonLegalities
	 */
	public ?ApiComMtgjsonLegalities $legalities = null;
	
	/**
	 * Starting life total modifier. Only exists for Vanguard cards.
	 * 
	 * @var ?integer
	 */
	public ?int $life = null;
	
	/**
	 * The loyalty of the card. This is only present for planeswalkers.
	 * 
	 * @var ?string
	 */
	public ?string $loyalty = null;
	
	/**
	 * The mana cost of this card. Consists of one or more mana symbols.
	 * 
	 * @var ?string
	 */
	public ?string $manaCost = null;
	
	/**
	 * The card name. For split, double-faced and flip cards, just the name of
	 * one side of the card. Basically each 'sub-card' has its own record.
	 * 
	 * @var ?string
	 */
	public ?string $name = null;
	
	/**
	 * The card number. This is printed at the bottom-center of the card in
	 * small text. This is a string, not an integer, because some cards have
	 * letters in their numbers.
	 * 
	 * @var ?string
	 */
	public ?string $number = null;
	
	/**
	 * The orientation of the card.
	 * 
	 * @var ?string
	 */
	public ?string $orientation = null;
	
	/**
	 * The uuid of the original printings of this card if it was rebalanced.
	 * 
	 * @var array<integer, UuidInterface>
	 */
	public array $originalPrintings = [];
	
	/**
	 * The original release date on the card at the time it was printed.
	 * 
	 * @var ?DateTimeInterface
	 */
	public ?DateTimeInterface $originalReleaseDate = null;
	
	/**
	 * The original text on the card at the time it was printed. This field is
	 * not available for promo cards.
	 * 
	 * @var ?string
	 */
	public ?string $originalText = null;
	
	/**
	 * The original type on the card at the time it was printed. This field is
	 * not available for promo cards.
	 * 
	 * @var ?string
	 */
	public ?string $originalType = null;
	
	/**
	 * List of uuid's of this card with counterparts, such as transformed or
	 * melded faces.
	 * 
	 * @var array<integer, UuidInterface>
	 */
	public array $otherFaceIds = [];
	
	/**
	 * The power of the card. This is only present for creatures. This is a
	 * string, not an integer, because some cards have powers like: "1+*".
	 * 
	 * @var ?string
	 */
	public ?string $power = null;
	
	/**
	 * The history of prices for this card.
	 * 
	 * @var ?ApiComMtgjsonCardPrice
	 */
	public ?ApiComMtgjsonCardPrice $prices = null;
	
	/**
	 * The sets that this card was printed in, expressed as an array of set
	 * codes.
	 * 
	 * @var array<integer, string>
	 */
	public array $printings = [];
	
	/**
	 * List of promotional types for a card.
	 * 
	 * @var array<integer, string>
	 */
	public array $promoTypes = [];
	
	/**
	 * Gets the purchase urls.
	 * 
	 * @var ?ApiComMtgjsonPurchaseUrls
	 */
	public ?ApiComMtgjsonPurchaseUrls $purchaseUrls = null;
	
	/**
	 * The rarity of the card. Examples: Common, Uncommon, Rare, Mythic Rare,
	 * Special, Basic Land.
	 * 
	 * @var ?string
	 */
	public ?string $rarity = null;
	
	/**
	 * The list of printings that are rebalanced versions of this card.
	 * 
	 * @var array<integer, UuidInterface>
	 */
	public array $rebalancedPrintings = [];
	
	/**
	 * For tokens, the names of the card that produced it.
	 * 
	 * @var ?ApiComMtgjsonRelatedCard
	 */
	public ?ApiComMtgjsonRelatedCard $relatedCards = null;
	
	/**
	 * For tokens, the names of the card that produced it.
	 * 
	 * @var array<integer, string>
	 */
	public array $reverseRelated = [];
	
	/**
	 * The rulings for the card. An array of objects, each object having
	 * 'date' and 'text' keys.
	 * 
	 * @var array<integer, ApiComMtgjsonRuling>
	 */
	public array $rulings = [];
	
	/**
	 * The security stamp of this card.
	 * 
	 * @var ?string
	 */
	public ?string $securityStamp = null;
	
	/**
	 * The set code that the card is from.
	 * 
	 * @var ?string
	 */
	public ?string $setCode = null;
	
	/**
	 * Identifier of the side. Used on cards with multiple faces, such as flip,
	 * split, transform cards. Can be a, b, or c.
	 * 
	 * @var ?string
	 */
	public ?string $side = null;
	
	/**
	 * The name of the artist that signed the card, if any.
	 * 
	 * @var ?string
	 */
	public ?string $signature = null;
	
	/**
	 * The source products where this card can be found.
	 * 
	 * @var ?ApiComMtgjsonSourceProduct
	 */
	public ?ApiComMtgjsonSourceProduct $sourceProducts = null;
	
	/**
	 * Sets the subsets this card appears on.
	 * 
	 * @var array<integer, string>
	 */
	public array $subsets = [];
	
	/**
	 * The subtypes of the card. These appear to the right of the dash in a
	 * card type. Usually each word is its own subtype. Example values: Trap,
	 * Arcane, Equipment, Aura, Human, Rat, Squirrel, etc.
	 * 
	 * @var array<integer, string>
	 */
	public array $subtypes = [];
	
	/**
	 * The supertypes of the card. These appear to the far left of the card
	 * type. Example values: Basic, Legendary, Snow, World, Ongoing.
	 * 
	 * @var array<integer, string>
	 */
	public array $supertypes = [];
	
	/**
	 * The text of the card. May contain mana symbols and other symbols.
	 * 
	 * @var ?string
	 */
	public ?string $text = null;
	
	/**
	 * The toughness of the card. This is only present for creatures. This is
	 * a string, not an integer, because some cards have toughness like: "1+*".
	 * 
	 * @var ?string
	 */
	public ?string $toughness = null;
	
	/**
	 * The card type. This is the type you would see on the card if printed
	 * today. Note: The dash is a UTF8 'long dash' as per the MTG rules.
	 * 
	 * @var ?string
	 */
	public ?string $type = null;
	
	/**
	 * The types of the card. These appear to the left of the dash in a card
	 * type. Example values: Instant, Sorcery, Artifact, Creature, Enchantment,
	 * Land, Planeswalker.
	 * 
	 * @var array<integer, string>
	 */
	public array $types = [];
	
	/**
	 * A universal unique ID (v5) generated by MTGJSON. Each entry is unique.
	 * 
	 * @var UuidInterface
	 */
	public ?UuidInterface $uuid = null;
	
	/**
	 * If a card has alternate art (for example, 4 different Forests, or the 2
	 * Brothers Yamazaki) then each other variation's multiverseid will be
	 * listed here, NOT including the current card's multiverseid. NOTE: Only
	 * present for sets that exist on Gatherer.
	 * 
	 * @var array<integer, UuidInterface>
	 */
	public array $variations = [];
	
	/**
	 * The watermark on the card. Note: Split cards don't currently have this
	 * field set, despite having a watermark on each side of the split card.
	 * 
	 * @var ?string
	 */
	public ?string $watermark = null;
	
}
