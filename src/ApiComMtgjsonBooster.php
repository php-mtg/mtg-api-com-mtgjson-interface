<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgjson-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgjson;

/**
 * ApiComMtgjsonBooster class file.
 * 
 * This represents a booster contents.
 * 
 * @author Anastaszor
 */
class ApiComMtgjsonBooster
{
	
	/**
	 * The contents of the booster.
	 * 
	 * @var array<string, int>
	 */
	public array $contents = [];
	
	/**
	 * The weight of the booster.
	 * 
	 * @var ?integer
	 */
	public ?int $weight = null;
	
}
