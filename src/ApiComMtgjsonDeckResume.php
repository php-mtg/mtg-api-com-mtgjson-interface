<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgjson-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgjson;

use DateTimeInterface;

/**
 * MtgjsonApiDeckResume class file.
 * 
 * This represents the metadata about the deck.
 * 
 * @author Anastaszor
 */
class ApiComMtgjsonDeckResume
{
	
	/**
	 * Gets the extension code of the deck.
	 * 
	 * @var ?string
	 */
	public ?string $code = null;
	
	/**
	 * Gets the name of the file.
	 * 
	 * @var ?string
	 */
	public ?string $fileName = null;
	
	/**
	 * Gets the name of the deck resume.
	 * 
	 * @var ?string
	 */
	public ?string $name = null;
	
	/**
	 * The type of deck.
	 * 
	 * @var ?string
	 */
	public ?string $type = null;
	
	/**
	 * Gets the release date of the deck resume.
	 * 
	 * @var ?DateTimeInterface
	 */
	public ?DateTimeInterface $releaseDate = null;
	
}
