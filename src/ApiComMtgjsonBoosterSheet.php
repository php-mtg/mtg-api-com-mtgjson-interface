<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgjson-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgjson;

/**
 * ApiComMtgjsonBoosterSheet class file.
 * 
 * This represents an unique sheet of cards for a booster.
 * 
 * @author Anastaszor
 */
class ApiComMtgjsonBoosterSheet
{
	
	/**
	 * Whether this sheet allows duplicates.
	 * 
	 * @var ?boolean
	 */
	public ?bool $allowDuplicates = null;
	
	/**
	 * Whether the sheet has balanced colors.
	 * 
	 * @var ?boolean
	 */
	public ?bool $balanceColors = null;
	
	/**
	 * The list of weights for each cards (uuid => weight).
	 * 
	 * @var array<string, integer>
	 */
	public array $cards = [];
	
	/**
	 * Whether the sheet contains a fixed (non random) set of cards.
	 * 
	 * @var ?boolean
	 */
	public ?bool $fixed = null;
	
	/**
	 * Whether the sheet contains foil cards.
	 * 
	 * @var ?boolean
	 */
	public ?bool $foil = null;
	
	/**
	 * The total of the weights in this sheet.
	 * 
	 * @var ?integer
	 */
	public ?int $totalWeight = null;
	
}
