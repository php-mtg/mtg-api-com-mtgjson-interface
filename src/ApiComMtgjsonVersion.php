<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgjson-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgjson;

use DateTimeInterface;
use PhpExtended\Version\VersionInterface;

/**
 * ApiComMtgjsonVersion class file.
 * 
 * This represents a version object from mtgjson.
 * 
 * @author Anastaszor
 */
class ApiComMtgjsonVersion
{
	
	/**
	 * The version number.
	 * 
	 * @var ?VersionInterface
	 */
	public ?VersionInterface $version = null;
	
	/**
	 * The date when this version was launched.
	 * 
	 * @var ?DateTimeInterface
	 */
	public ?DateTimeInterface $date = null;
	
}
