<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgjson-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgjson;

/**
 * ApiComMtgjsonSealedProductContentPack class file.
 * 
 * This  represents a single sealed pack.
 * 
 * @author Anastaszor
 */
class ApiComMtgjsonSealedProductContentPack
{
	
	/**
	 * The code of the related content Pack.
	 * 
	 * @var ?string
	 */
	public ?string $code = null;
	
	/**
	 * The code of the related set.
	 * 
	 * @var ?string
	 */
	public ?string $set = null;
	
}
