<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgjson-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgjson;

use Psr\Http\Message\UriInterface;

/**
 * ApiComMtgjsonPurchaseUrls class file.
 * 
 * This represents all the affiliated links for the given card.
 * 
 * @author Anastaszor
 */
class ApiComMtgjsonPurchaseUrls
{
	
	/**
	 * The url to purchase the card on cardkingdom website.
	 *
	 * @var ?UriInterface
	 */
	public ?UriInterface $cardKingdom = null;
	
	/**
	 * The url to purchase the card foil on cardkingdom website.
	 * 
	 * @var ?UriInterface
	 */
	public ?UriInterface $cardKingdomFoil = null;
	
	/**
	 * The url to purchase the card foil etched on cardkingdom website.
	 * 
	 * @var ?UriInterface
	 */
	public ?UriInterface $cardKingdomEtched = null;
	
	/**
	 * The url to purchase the card on the cardmarket website.
	 * 
	 * @var ?UriInterface
	 */
	public ?UriInterface $cardmarket = null;
	
	/**
	 * The url to purchase the card on the tcgplayer website.
	 * 
	 * @var ?UriInterface
	 */
	public ?UriInterface $tcgplayer = null;
	
	/**
	 * The url to purchase the etched card on the tcgplayer website.
	 * 
	 * @var ?UriInterface
	 */
	public ?UriInterface $tcgplayerEtched = null;
	
}
