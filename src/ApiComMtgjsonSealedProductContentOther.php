<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgjson-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgjson;

/**
 * ApiComMtgjsonSealedProductContentOther class file.
 * 
 * This  represents a sealed product other.
 * 
 * @author Anastaszor
 */
class ApiComMtgjsonSealedProductContentOther
{
	
	/**
	 * The name of this content Other.
	 * 
	 * @var ?string
	 */
	public ?string $name = null;
	
}
