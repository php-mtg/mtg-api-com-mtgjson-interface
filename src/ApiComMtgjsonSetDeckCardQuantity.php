<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgjson-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgjson;

use PhpExtended\Uuid\UuidInterface;

/**
 * ApiComMtgjsonSetDeckCardQuantity class file.
 * 
 * This represents a card quantity and quality over a given deck tied as a set
 * as a sealed product.
 * 
 * @author Anastaszor
 */
class ApiComMtgjsonSetDeckCardQuantity
{
	
	/**
	 * The count quantity of the selected card.
	 * 
	 * @var ?int
	 */
	public ?int $count = null;
	
	/**
	 * The finish of the selected card.
	 * 
	 * @var ?string
	 */
	public ?string $finish = null;
	
	/**
	 * The uuid of the related card.
	 * 
	 * @var ?UuidInterface
	 */
	public ?UuidInterface $uuid = null;
	
}
