<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgjson-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgjson;

use DateTimeInterface;
use PhpExtended\Uuid\UuidInterface;

/**
 * ApiComMtgjsonSealedProduct class file.
 * 
 * This represents a sealed product from a set list.
 * 
 * @author Anastaszor
 */
class ApiComMtgjsonSealedProduct
{
	
	/**
	 * The category of this product.
	 * 
	 * @var ?string
	 */
	public ?string $category = null;
	
	/**
	 * The card count of this sealed product.
	 * 
	 * @var ?integer
	 */
	public ?int $cardCount = null;
	
	/**
	 * The contents of this sealed product.
	 * 
	 * @var ?ApiComMtgjsonSealedProductContent
	 */
	public ?ApiComMtgjsonSealedProductContent $contents = null;
	
	/**
	 * The identifiers.
	 * 
	 * @var ?ApiComMtgjsonIdentifier
	 */
	public ?ApiComMtgjsonIdentifier $identifiers = null;
	
	/**
	 * The name.
	 * 
	 * @var ?string
	 */
	public ?string $name = null;
	
	/**
	 * The size of the product.
	 * 
	 * @var ?integer
	 */
	public ?int $productSize = null;
	
	/**
	 * The purchase uris.
	 * 
	 * @var ?ApiComMtgjsonPurchaseUrls
	 */
	public ?ApiComMtgjsonPurchaseUrls $purchaseUrls = null;
	
	/**
	 * The release date.
	 * 
	 * @var ?DateTimeInterface
	 */
	public ?DateTimeInterface $releaseDate = null;
	
	/**
	 * The subtype.
	 * 
	 * @var ?string
	 */
	public ?string $subtype = null;
	
	/**
	 * The uuid.
	 * 
	 * @var UuidInterface
	 */
	public ?UuidInterface $uuid = null;
	
}
