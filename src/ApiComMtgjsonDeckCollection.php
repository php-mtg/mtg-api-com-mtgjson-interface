<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgjson-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgjson;

/**
 * MtgjsonApiDeckCollection class file.
 * 
 * This represents the collection of decks.
 * 
 * @author Anastaszor
 */
class ApiComMtgjsonDeckCollection
{
	
	/**
	 * Gets the version of the deck collection.
	 * 
	 * @var ?ApiComMtgjsonVersion
	 */
	public ?ApiComMtgjsonVersion $meta = null;
	
	/**
	 * Gets the deck list.
	 * 
	 * @var array<integer, ApiComMtgjsonDeckResume>
	 */
	public array $data = [];
	
}
