<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgjson-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgjson;

use DateTimeInterface;

/**
 * ApiComMtgjsonSet class file.
 * 
 * This represents a set information with all the cards printed with
 * the set.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.TooManyFields")
 */
class ApiComMtgjsonSet
{
	
	/**
	 * Number of cards in the set. Will default to totalSetSize if not
	 * available. Note that Wizards sometimes prints extra cards beyond the
	 * set size into promos or supplemental products.
	 * 
	 * @var ?integer
	 */
	public ?int $baseSetSize = null;
	
	/**
	 * The block the set is in.
	 * 
	 * @var ?string
	 */
	public ?string $block = null;
	
	/**
	 * The contents of a booster of the set, if exists.
	 * 
	 * @var array<string, ApiComMtgjsonBoosterPolicy>
	 */
	public array $booster = [];
	
	/**
	 * The cards of the set.
	 * 
	 * @var array<integer, ApiComMtgjsonCard>
	 */
	public array $cards = [];
	
	/**
	 * The cardsphere id.
	 * 
	 * @var ?integer
	 */
	public ?int $cardsphereSetId = null;
	
	/**
	 * The code of the set.
	 * 
	 * @var string
	 */
	public ?string $code = null;
	
	/**
	 * Alternate set code Wizards uses for a select few duel deck sets.
	 * 
	 * @var ?string
	 */
	public ?string $codeV3 = null;
	
	/**
	 * The decks of the set.
	 *
	 * @var array<integer, ApiComMtgjsonSetDeck>
	 */
	public array $decks = [];
	
	/**
	 * Is this set available only outside the United States?
	 * 
	 * @var ?boolean
	 */
	public ?bool $isForeignOnly = null;
	
	/**
	 * Is the set available only in foil?
	 * 
	 * @var ?boolean
	 */
	public ?bool $isFoilOnly = null;
	
	/**
	 * If this set is only available in non-foil.
	 * 
	 * @var ?boolean
	 */
	public ?bool $isNonFoilOnly = null;
	
	/**
	 * Whether this set was distributed only online.
	 * 
	 * @var ?boolean
	 */
	public ?bool $isOnlineOnly = null;
	
	/**
	 * If this set is available only in paper.
	 * 
	 * @var ?boolean
	 */
	public ?bool $isPaperOnly = null;
	
	/**
	 * Whether this set is currently in preview mode.
	 * 
	 * @var ?boolean
	 */
	public ?bool $isPartialPreview = null;
	
	/**
	 * The matching keyrune code for Keyrune image icons. 
	 * https://andrewgioia.github.io/Keyrune/.
	 * 
	 * @var ?string
	 */
	public ?string $keyruneCode = null;
	
	/**
	 * The languages supported by this set.
	 * 
	 * @var array<integer, string>
	 */
	public array $languages = [];
	
	/**
	 * The name of the set on the magiccardmarket.eu website, if exists.
	 * 
	 * @var ?string
	 */
	public ?string $mcmName = null;
	
	/**
	 * The id of the set on the magiccardmarket.eu website, if exists.
	 * 
	 * @var ?integer
	 */
	public ?int $mcmId = null;
	
	/**
	 * The id of the extra set.
	 * 
	 * @var ?integer
	 */
	public ?int $mcmIdExtras = null;
	
	/**
	 * Set code for the set as it appears on Magic: The Gathering Online.
	 * 
	 * @var ?string
	 */
	public ?string $mtgoCode = null;
	
	/**
	 * The name of the set.
	 * 
	 * @var ?string
	 */
	public ?string $name = null;
	
	/**
	 * The parent set code for set variations like promotions, guild kits, etc.
	 * 
	 * @var ?string
	 */
	public ?string $parentCode = null;
	
	/**
	 * The release date of the set.
	 * 
	 * @var ?DateTimeInterface
	 */
	public ?DateTimeInterface $releaseDate = null;
	
	/**
	 * The sealed products.
	 *
	 * @var array<integer, ApiComMtgjsonSealedProduct>
	 */
	public array $sealedProduct = [];
	
	/**
	 * Group ID of the set on TCGPlayer.
	 * 
	 * @var ?string
	 */
	public ?string $tcgplayerGroupId = null;
	
	/**
	 * Tokens available to the set. See the token structure.
	 * 
	 * @var array<integer, ApiComMtgjsonCard>
	 */
	public array $tokens = [];
	
	/**
	 * The set of the tokens.
	 * 
	 * @var ?string
	 */
	public ?string $tokenSetCode = null;
	
	/**
	 * Total number of cards in the set, including promos and related
	 * supplemental products.
	 * 
	 * @var ?integer
	 */
	public ?int $totalSetSize = null;
	
	/**
	 * The translations of the set name.
	 * 
	 * @var ?ApiComMtgjsonSetTranslation
	 */
	public ?ApiComMtgjsonSetTranslation $translations = null;
	
	/**
	 * As of 03-28-2019, the possible values are archenemy, box, core,
	 * commander, draft_innovation, duel_deck, expansion, from_the_vault,
	 * funny, masters, masterpiece, memorabilia, spellbook, planechase,
	 * premium_deck, promo, starter, token, treasure_chest, or vanguard.
	 * 
	 * @var ?string
	 */
	public ?string $type = null;
	
}
