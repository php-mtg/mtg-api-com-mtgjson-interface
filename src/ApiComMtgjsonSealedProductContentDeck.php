<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgjson-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgjson;

/**
 * ApiComMtgjsonSealedProductContentDeck class file.
 * 
 * This represents a sealed deck.
 * 
 * @author Anastaszor
 */
class ApiComMtgjsonSealedProductContentDeck
{
	
	/**
	 * The name of this content Deck.
	 * 
	 * @var ?string
	 */
	public ?string $name = null;
	
	/**
	 * The code of the related set.
	 * 
	 * @var ?string
	 */
	public ?string $set = null;
	
}
