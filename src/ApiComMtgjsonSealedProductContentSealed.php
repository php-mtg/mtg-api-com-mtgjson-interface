<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgjson-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgjson;

use PhpExtended\Uuid\UuidInterface;

/**
 * ApiComMtgjsonSealedProductContentSealed class file.
 * 
 * This  represents a single sealed container.
 * 
 * @author Anastaszor
 */
class ApiComMtgjsonSealedProductContentSealed
{
	
	/**
	 * The quantity of this content Sealed.
	 * 
	 * @var ?int
	 */
	public ?int $count = null;
	
	/**
	 * The name of this content Sealed.
	 * 
	 * @var ?string
	 */
	public ?string $name = null;
	
	/**
	 * The code of the related set.
	 * 
	 * @var ?string
	 */
	public ?string $set = null;
	
	/**
	 * The uuid of this content Sealed.
	 * 
	 * @var ?UuidInterface
	 */
	public ?UuidInterface $uuid = null;
	
}
