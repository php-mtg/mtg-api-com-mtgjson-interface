<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgjson-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgjson;

use DateTimeInterface;

/**
 * ApiComMtgjsonSetResume class file.
 * 
 * This represents a short vision of a set's information.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.TooManyFields")
 */
class ApiComMtgjsonSetResume
{
	
	/**
	 * The name of the set.
	 * 
	 * @var ?string
	 */
	public ?string $name = null;
	
	/**
	 * The block of the set.
	 * 
	 * @var ?string
	 */
	public ?string $block = null;
	
	/**
	 * The code of the set.
	 * 
	 * @var string
	 */
	public ?string $code = null;
	
	/**
	 * The decks of the set.
	 * 
	 * @var array<integer, ApiComMtgjsonSetDeck>
	 */
	public array $decks = [];
	
	/**
	 * The languages.
	 * 
	 * @var array<integer, string>
	 */
	public array $languages = [];
	
	/**
	 * The magiccardmarket id.
	 * 
	 * @var ?integer
	 */
	public ?int $mcmId = null;
	
	/**
	 * The magiccardmarket extra ids.
	 * 
	 * @var ?integer
	 */
	public ?int $mcmIdExtras = null;
	
	/**
	 * The magiccardmarket name.
	 * 
	 * @var ?string
	 */
	public ?string $mcmName = null;
	
	/**
	 * The mtgo code.
	 * 
	 * @var ?string
	 */
	public ?string $mtgoCode = null;
	
	/**
	 * The tcgplayer group id.
	 * 
	 * @var ?integer
	 */
	public ?int $tcgplayerGroupId = null;
	
	/**
	 * The cardsphere set id.
	 * 
	 * @var ?integer
	 */
	public ?int $cardsphereSetId = null;
	
	/**
	 * The code of the parent set, if any.
	 * 
	 * @var ?string
	 */
	public ?string $parentCode = null;
	
	/**
	 * The release date of the set.
	 * 
	 * @var ?DateTimeInterface
	 */
	public ?DateTimeInterface $releaseDate = null;
	
	/**
	 * Gets the base set size (which is written on the regular cards), in nb of cards.
	 * 
	 * @var ?integer
	 */
	public ?int $baseSetSize = null;
	
	/**
	 * Gets the total set size, in nb of cards.
	 * 
	 * @var ?integer
	 */
	public ?int $totalSetSize = null;
	
	/**
	 * Whether this set is currently in preview mode.
	 *
	 * @var ?boolean
	 */
	public ?bool $isPartialPreview = null;
	
	/**
	 * Whether this set was not released in english.
	 * 
	 * @var ?boolean
	 */
	public ?bool $isForeignOnly = null;
	
	/**
	 * Whether this set is foil only.
	 * 
	 * @var ?boolean
	 */
	public ?bool $isFoilOnly = null;
	
	/**
	 * Whether this set is non foil only.
	 * 
	 * @var ?boolean
	 */
	public ?bool $isNonFoilOnly = null;
	
	/**
	 * Whether this set is online only.
	 * 
	 * @var ?boolean
	 */
	public ?bool $isOnlineOnly = null;
	
	/**
	 * The keyrune code of this set.
	 * 
	 * @var ?string
	 */
	public ?string $keyruneCode = null;
	
	/**
	 * The sealed products.
	 * 
	 * @var array<integer, ApiComMtgjsonSealedProduct>
	 */
	public array $sealedProduct = [];
	
	/**
	 * The set code of tokens, if any.
	 * 
	 * @var ?string
	 */
	public ?string $tokenSetCode = null;
	
	/**
	 * The translations of this set.
	 * 
	 * @var ?ApiComMtgjsonSetTranslation
	 */
	public ?ApiComMtgjsonSetTranslation $translations = null;
	
	/**
	 * Gets the type of the set resume.
	 * 
	 * @var ?string
	 */
	public ?string $type = null;
	
}
