<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgjson-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgjson;

/**
 * ApiComMtgjsonLegalities class file.
 * 
 * This represents the legalities for a given card.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.TooManyFields")
 */
class ApiComMtgjsonLegalities
{
	
	/**
	 * Whether this card is legal in alchemy.
	 * 
	 * @var ?boolean
	 */
	public ?bool $alchemy = null;
	
	/**
	 * Whether this card is legal in brawl.
	 * 
	 * @var ?boolean
	 */
	public ?bool $brawl = null;
	
	/**
	 * Whether this card is legal in commander.
	 * 
	 * @var ?boolean
	 */
	public ?bool $commander = null;
	
	/**
	 * Whether this card is legal in duel commander.
	 * 
	 * @var ?boolean
	 */
	public ?bool $duel = null;
	
	/**
	 * Whether this card is legal in explorer.
	 * 
	 * @var ?boolean
	 */
	public ?bool $explorer = null;
	
	/**
	 * Whether this card is legal in future play.
	 *
	 * @var ?boolean
	 */
	public ?bool $future = null;
	
	/**
	 * Whether this card is legal in gladiator.
	 *
	 * @var ?boolean
	 */
	public ?bool $gladiator = null;
	
	/**
	 * Whether this card is legal in historic.
	 * 
	 * @var ?boolean
	 */
	public ?bool $historic = null;
	
	/**
	 * Whether this card is legal in historic brawl.
	 * 
	 * @var ?boolean
	 */
	public ?bool $historicbrawl = null;
	
	/**
	 * Whether this card is legal in legacy.
	 * 
	 * @var ?boolean
	 */
	public ?bool $legacy = null;
	
	/**
	 * Whether this card is legal in modern.
	 * 
	 * @var ?boolean
	 */
	public ?bool $modern = null;
	
	/**
	 * Whether this card is legal in oathbreaker.
	 * 
	 * @var ?boolean
	 */
	public ?bool $oathbreaker = null;
	
	/**
	 * Whether this card is legal in old school.
	 * 
	 * @var ?boolean
	 */
	public ?bool $oldschool = null;
	
	/**
	 * Whether this card is legal in pauper.
	 * 
	 * @var ?boolean
	 */
	public ?bool $pauper = null;
	
	/**
	 * Whether this card is legal in pauper commander.
	 * 
	 * @var ?boolean
	 */
	public ?bool $paupercommander = null;
	
	/**
	 * Whether this card is legal in penny.
	 * 
	 * @var ?boolean
	 */
	public ?bool $penny = null;
	
	/**
	 * Whether this card is legal in pioneer.
	 * 
	 * @var ?boolean
	 */
	public ?bool $pioneer = null;
	
	/**
	 * Whether this card is legal in predh.
	 * 
	 * @var ?boolean
	 */
	public ?bool $predh = null;
	
	/**
	 * Whether this card is legal in premodern.
	 * 
	 * @var ?boolean
	 */
	public ?bool $premodern = null;
	
	/**
	 * Whether this card is legal in standard.
	 * 
	 * @var ?boolean
	 */
	public ?bool $standard = null;
	
	/**
	 * Whether this card is legal in vintage.
	 * 
	 * @var ?boolean
	 */
	public ?bool $vintage = null;
	
}
