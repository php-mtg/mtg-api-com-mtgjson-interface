<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgjson-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgjson;

use PhpExtended\DataProvider\UnprovidableThrowable;
use PhpExtended\Reifier\ReificationThrowable;
use Psr\Http\Client\ClientExceptionInterface;
use Stringable;

/**
 * ApiComMtgjsonEndpointInterface interface file.
 *
 * This represents the mtgjson.com website's api.
 *
 * @author Anastaszor
 */
interface ApiComMtgjsonEndpointInterface extends Stringable
{
	
	/**
	 * Gets a resume of each existing deck known to the api.
	 *
	 * @return ApiComMtgjsonDeckCollection
	 * @throws ClientExceptionInterface
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getDeckList() : ApiComMtgjsonDeckCollection;
	
	/**
	 * Gets the deck information with all the cards of the deck.
	 *
	 * @param string $deckFilePath
	 * @return ApiComMtgjsonDeckMeta
	 * @throws ClientExceptionInterface
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getDeck(string $deckFilePath) : ApiComMtgjsonDeckMeta;
	
	/**
	 * Gets a resume of each existing set known to the api.
	 *
	 * @return ApiComMtgjsonSetCollection
	 * @throws ClientExceptionInterface
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getSetList() : ApiComMtgjsonSetCollection;
	
	/**
	 * Gets the set information with all the cards of the set.
	 *
	 * @param string $setCode
	 * @return ApiComMtgjsonSetMeta
	 * @throws ClientExceptionInterface
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getSet(string $setCode) : ApiComMtgjsonSetMeta;
	
}
