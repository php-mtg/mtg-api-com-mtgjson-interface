<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgjson-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgjson;

/**
 * ApiComMtgjsonCardTypeCollection class file.
 * 
 * This represents the collection of card types and their dependancies.
 * 
 * @author Anastaszor
 */
class ApiComMtgjsonCardTypeCollection
{
	
	/**
	 * The metadata about this card type collection.
	 * 
	 * @var ?ApiComMtgjsonVersion
	 */
	public ?ApiComMtgjsonVersion $meta = null;
	
	/**
	 * The types of this card type collection.
	 * 
	 * @var array<integer, ApiComMtgjsonCardType>
	 */
	public array $types = [];
	
}
