<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgjson-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgjson;

/**
 * ApiComMtgjsonSealedProductContent class file.
 * 
 * This represents a single sealed product.
 * 
 * @author Anastaszor
 */
class ApiComMtgjsonSealedProductContent
{
	
	/**
	 * The card contents.
	 * 
	 * @var array<integer, ApiComMtgjsonSealedProductContentCard>
	 */
	public array $card = [];
	
	/**
	 * The deck contents.
	 * 
	 * @var array<integer, ApiComMtgjsonSealedProductContentDeck>
	 */
	public array $deck = [];
	
	/**
	 * The other contents.
	 * 
	 * @var array<integer, ApiComMtgjsonSealedProductContentOther>
	 */
	public array $other = [];
	
	/**
	 * The sealed contents.
	 * 
	 * @var array<integer, ApiComMtgjsonSealedProductContentSealed>
	 */
	public array $sealed = [];
	
	/**
	 * The pack contents.
	 * 
	 * @var array<integer, ApiComMtgjsonSealedProductContentPack>
	 */
	public array $pack = [];
	
	/**
	 * The variable contents.
	 * 
	 * @var array<integer, ApiComMtgjsonSealedProductContentVariable>
	 */
	public array $variable = [];
	
}
