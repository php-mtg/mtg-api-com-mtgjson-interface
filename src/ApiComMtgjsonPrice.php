<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgjson-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgjson;

use DateTimeInterface;

/**
 * ApiComMtgjsonPrice class file.
 * 
 * This represents a single price for a single card in a single environment.
 * 
 * @author Anastaszor
 */
class ApiComMtgjsonPrice
{
	
	/**
	 * The date of the price.
	 * 
	 * @var ?DateTimeInterface
	 */
	public ?DateTimeInterface $date = null;
	
	/**
	 * The value of the price.
	 * 
	 * @var ?float
	 */
	public ?float $value = null;
	
}
