<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgjson-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgjson;

/**
 * ApiComMtgjsonForeignData class file.
 * 
 * This represents all foreign (i.e. non english) information about a
 * card into a specific language.
 * 
 * @author Anastaszor
 */
class ApiComMtgjsonForeignData
{
	
	/**
	 * The name of the face.
	 * 
	 * @var ?string
	 */
	public ?string $faceName = null;
	
	/**
	 * Flavor text in foreign language.
	 * 
	 * @var ?string
	 */
	public ?string $flavorText = null;
	
	/**
	 * The language of the foreign information.
	 * 
	 * @var ?string
	 */
	public ?string $language = null;
	
	/**
	 * Multiverse ID of the card. Omitted if not available.
	 * 
	 * @var ?integer
	 */
	public ?int $multiverseId = null;
	
	/**
	 * The name of the card in that language.
	 * 
	 * @var ?string
	 */
	public ?string $name = null;
	
	/**
	 * Text ruling of the card in foreign language.
	 * 
	 * @var ?string
	 */
	public ?string $text = null;
	
	/**
	 * Type of the card. Includes any supertypes and subtypes.
	 * 
	 * @var ?string
	 */
	public ?string $type = null;
	
}
