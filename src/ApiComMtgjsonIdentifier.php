<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgjson-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgjson;

use PhpExtended\Uuid\UuidInterface;

/**
 * ApiComMtgjsonIdentifier class file.
 * 
 * This represents the multiple identifiers that may be used by other vendors
 * to represent the same card, or printing, or some model near the concept of
 * a single card.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.TooManyFields")
 */
class ApiComMtgjsonIdentifier
{
	
	/**
	 * The cardkingdom id.
	 * 
	 * @var ?integer
	 */
	public ?int $cardKingdomId = null;
	
	/**
	 * The cardkingdom foil id.
	 * 
	 * @var ?integer
	 */
	public ?int $cardKingdomFoilId = null;
	
	/**
	 * The cardkingdom foil id.
	 * 
	 * @var ?integer
	 */
	public ?int $cardKingdomEtchedId = null;
	
	/**
	 * The cardsphere id.
	 * 
	 * @var ?integer
	 */
	public ?int $cardsphereId = null;
	
	/**
	 * The mcm id.
	 * 
	 * @var ?string
	 */
	public ?string $mcmId = null;
	
	/**
	 * The mcm meta id.
	 * 
	 * @var ?string
	 */
	public ?string $mcmMetaId = null;
	
	/**
	 * The mtg arena id.
	 * 
	 * @var ?int
	 */
	public ?int $mtgArenaId = null;
	
	/**
	 * The mtgo id.
	 * 
	 * @var ?string
	 */
	public ?string $mtgoId = null;
	
	/**
	 * The mtgo foil id.
	 * 
	 * @var ?string
	 */
	public ?string $mtgoFoilId = null;
	
	/**
	 * The mtgjson v4 id.
	 * 
	 * @var ?UuidInterface
	 */
	public ?UuidInterface $mtgjsonV4Id = null;
	
	/**
	 * The mtgjson foil version id.
	 *
	 * @var ?UuidInterface
	 */
	public ?UuidInterface $mtgjsonFoilVersionId = null;
	
	/**
	 * The mtgjson nonfoil version id.
	 *
	 * @var ?UuidInterface
	 */
	public ?UuidInterface $mtgjsonNonFoilVersionId = null;
	
	/**
	 * The multiverse id.
	 * 
	 * @var ?int
	 */
	public ?int $multiverseId = null;
	
	/**
	 * The scryfall id.
	 * 
	 * @var ?UuidInterface
	 */
	public ?UuidInterface $scryfallId = null;
	
	/**
	 * The scryfall oracle id.
	 * 
	 * @var ?UuidInterface
	 */
	public ?UuidInterface $scryfallOracleId = null;
	
	/**
	 * The scryfall illustration id.
	 * 
	 * @var ?UuidInterface
	 */
	public ?UuidInterface $scryfallIllustrationId = null;
	
	/**
	 * The tcg player id for the card.
	 * 
	 * @var ?integer
	 */
	public ?int $tcgplayerProductId = null;
	
	/**
	 * The tcg player etched product id for this card.
	 * 
	 * @var ?integer
	 */
	public ?int $tcgplayerEtchedProductId = null;
	
}
