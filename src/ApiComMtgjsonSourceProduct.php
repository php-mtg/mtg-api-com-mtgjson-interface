<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgjson-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgjson;

use PhpExtended\Uuid\UuidInterface;

/**
 * ApiComMtgjsonSourceProduct class file.
 * 
 * This class represents the relations between a card and a product it can be
 * found in.
 * 
 * @author Anastaszor
 */
class ApiComMtgjsonSourceProduct
{
	
	/**
	 * The id of the source product where this card can be found in foil.
	 * 
	 * @var array<integer, UuidInterface>
	 */
	public array $foil = [];
	
	/**
	 * The id of the source product where this card can be found in non foil.
	 * 
	 * @var array<integer, UuidInterface>
	 */
	public array $nonfoil = [];
	
	/**
	 * The id of the source product where this card can be found in etched foil.
	 * 
	 * @var array<integer, UuidInterface>
	 */
	public array $etched = [];
	
}
