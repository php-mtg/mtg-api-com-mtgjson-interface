<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/mtg-api-com-mtgjson-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\ApiComMtgjson;

use DateTimeInterface;

/**
 * ApiComMtgjsonDeck class file.
 * 
 * This represents a deck with full cards lists.
 * 
 * @author Anastaszor
 */
class ApiComMtgjsonDeck
{
	
	/**
	 * The extension code of the deck.
	 * 
	 * @var ?string
	 */
	public ?string $code = null;
	
	/**
	 * The cards of the commanders.
	 *
	 * @var array<integer, ApiComMtgjsonCard>
	 */
	public array $commander = [];
	
	/**
	 * The file name of the deck.
	 * 
	 * @var ?string
	 */
	public ?string $fileName = null;
	
	/**
	 * The cards of the main board.
	 *
	 * @var array<integer, ApiComMtgjsonCard>
	 */
	public array $mainBoard = [];
	
	/**
	 * The name of the deck.
	 * 
	 * @var ?string
	 */
	public ?string $name = null;
	
	/**
	 * The release date of the deck.
	 * 
	 * @var ?DateTimeInterface
	 */
	public ?DateTimeInterface $releaseDate = null;
	
	/**
	 * The cards of the side board.
	 *
	 * @var array<integer, ApiComMtgjsonCard>
	 */
	public array $sideBoard = [];
	
	/**
	 * The type of deck.
	 * 
	 * @var ?string
	 */
	public ?string $type = null;
	
}
