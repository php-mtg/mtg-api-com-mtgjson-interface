# php-mtg/mtg-api-com-mtgjson-interface

A library that gets data from the mtgjson.com website's api.

![coverage](https://gitlab.com/php-mtg/mtg-api-com-mtgjson-interface/badges/master/pipeline.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar install php-mtg/mtg-api-com-mtgjson-interface ^8`


## Basic Usage

This library is an interface-only library.

For a concrete implementation, see [`php-mtg/mtg-api-com-mtgjson-object`](https://gitlab.com/php-mtg/mtg-api-com-mtgjson-object).


## License

MIT (See [license file](LICENSE)).
